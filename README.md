Grumpy Girl Post
Welcome page, signup, card choices, preferences
via mongolab-meteor-leadcapture-app
===============================

[ ![Codeship Status for biomassives/mongolab-meteor-leadcapture-app](https://codeship.io/projects/cca6e3b0-2d01-0132-bca9-624c68ac6a26/status)](https://codeship.io/projects/39090)

work in this branch is being done to add features for stormpath user authorization and sendgrid mail service.
=== - greg @ biomassiv.es

Sample app & blog post for [MongoLab](http://mongolab.com) / [Meteor](http://meteor.com) integration

This is a simple lead form / CRM for a website. There will be two views:

* Entry view - similar to the lead form on [FrozenRidge.co](http://frozenridge.co)
* Admin view - tablular view of leads

Views will utilize Meteor to update live, in realtime. Backend will be MongoLab.


## Usage

You need to have meteor installed:

`curl https://install.meteor.com | sh`

Now run the app from the `app` directory:

`cd app; meteor`

You should be able to access the app at http://localhost:3000/

```
$ meteor
[[[[[ ~/projects/node/mongolab-meteor-leadcapture-app/app ]]]]]

=> Meteor server running on: http://localhost:3000/
```

## Bundling

To deploy this so it can run with MongoLab, you need to generate the "bundle":

`meteor bundle sample.tar.gz`

This generates a tarball named `sample.tar.gz` which is essentially the meteor
app compiled to Node.JS.

This can then be run just like a normal Node.JS app (`npm install fibers && node main.js`). A MongoLab database can be used by creating a database at
http://mongolab.com and setting the environment variable MONGO_URL to the
MongoDB URI provided by MongoLab. Sandbox plans that give you .5GB of storage
are free.

## Blog Post

A blog post with a detailed walkthrough of the app is at the [MongoLab blog](http://blog.mongolab.com/2013/05/build-your-own-lead-capture-page-with-meteor-and-mongolab-in-minutes/)